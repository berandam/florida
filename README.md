# FLOrIDA #

FLOrIDA is a lightweight tool that extracts annotations from implementation artifacts, aggregates and processes them, and visualizes several different levels of abstracted views to the user. It also presents Feature and Project metrics metrics describing this relationship, like coupling between features and their implementing files, sizes of features, nesting of features, tangling and number of authors, are also presented.


## Note: ##
The tool is currently going through an approval process and will be made available as soon as possible.




### Contribution guidelines ###
Berima Andam
Prof. Thorsten Berger
Dr. Andreas Burger
Prof. Michel R. V. Chaudron

### Who do I talk to? ###
berima@student.chalmers.se